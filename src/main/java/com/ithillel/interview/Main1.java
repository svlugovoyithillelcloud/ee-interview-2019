package com.ithillel.interview;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

public class Main1 {
    public static void main(String[] args) {
        Employee employee =
                new Employee(12L, "Ivan", "Ivanov", LocalDate.of(1990, 10, 10), "IT");

        System.out.println(employee);
    }

}

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
class Employee {
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birth;
    private String department;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (!id.equals(employee.id)) return false;
        if (!firstName.equals(employee.firstName)) return false;
        if (!lastName.equals(employee.lastName)) return false;
        if (!birth.equals(employee.birth)) return false;
        return department.equals(employee.department);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + birth.hashCode();
        result = 31 * result + department.hashCode();
        return result;
    }
}


