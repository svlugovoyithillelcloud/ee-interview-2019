package com.ithillel.demoboot.repository;

import com.ithillel.demoboot.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    public List<Contact> findByCity(String city);

    @Query("select c from Contact c order by c.id desc")
    public List<Contact> findAllIdDesc();

}
