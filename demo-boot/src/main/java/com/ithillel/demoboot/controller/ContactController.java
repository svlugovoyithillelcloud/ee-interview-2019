package com.ithillel.demoboot.controller;

import com.ithillel.demoboot.model.Contact;
import com.ithillel.demoboot.service.ContactService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/contacts")
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    //todo - GET - /api/v1/contacts/
    public List<Contact> getAll() {
        return null;
    }

    //todo - GET - /api/v1/contacts/1
    public Contact getById(Long id) {
        return null;
    }

    //todo - POST, http status 201
    public Contact saveNew(Contact contact) {
        return null;
    }

}
